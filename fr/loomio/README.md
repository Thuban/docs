# Framavox

[Framavox](https://framavox.org) est un service en ligne de prise de décision proposé par l'association Framasoft.

Le service repose sur le logiciel libre [Framavox](https://loomio.org)

Voici une documentation pour vous aider à l'utiliser.
Il s’agit de la traduction française de la [documentation officielle](https://loomio.gitbooks.io/manual/content/en/index.html) réalisée par l'équipe [Framalang](https://participer.framasoft.org/traduction-rejoignez-framalang/).

## Loomio, c‎‎’est quoi&nbsp;?

Loomio est un outil en ligne, simple, facile à utiliser, qui permet des prises de décision collaborative. Loomio vous permet d‎‎’engager des discussions en y invitant les personnes concernées. Au moment opportun, Loomio vous permet de prendre des décisions et de transformer vos délibérations en actions dans le monde réel.

## Comment Loomio fonctionne-t-il&nbsp;?

* **Créez un groupe** pour commencer à collaborer sur Loomio
* **Invitez des personnes** à vous rejoindre dans votre groupe
* **Commencez une discussion** sur le sujet qui vous préoccupe
* **Faites une proposition** afin d‎‎’avoir une idée du ressenti de chacun
* **Décidez ensemble** - tout le monde peut donner son accord, s‎‎’abstenir, exprimer son désaccord, ou bloquer la proposition - vous aurez un résultat rapide et verrez en un coup d‎‎’oeil ce que chacun pense de la proposition.

[Regardez cette courte vidéo](https://framatube.org/media/tutoriel-framavox) qui vous montre comment utiliser Framavox.

---

## Table des matières

* [Avant de commencer](loomio/getting_started.md)
* [Réglages de groupe](loomio/group_settings.md)
* [Coordonner votre groupe](loomio/coordinating_your_group.md)
* [Inviter de nouveaux membres](loomio/inviting_new_members.md)
* [Discussion](loomio/discussion_threads.md)
* [Commentaires](loomio/comments.md)
* [Propositions](loomio/proposals.md)
* [Sous-groupes](loomio/subgroups.md)
* [Naviguer dans Loomio](loomio/reading_loomio.md)
* [Rester à jour](loomio/keeping_up_to_date.md)
* [Votre profil](loomio/your_user_profile.md)