# Framagit

Framagit est la forge logicielle de Framasoft reposant sur le logiciel [Gitlab](https://gitlab.com).

Elle est ouverte à tous, dans la limite de 42 projets par personne.
Les projets peuvent être publics ou privés.

Avec Gitlab, nous proposons aussi de l'intégration continue avec [GitlabCI](https://docs.gitlab.com/ce/ci/README.html) et l'hébergement de pages statiques avec [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/index.html) (voir notre [documentation](gitlab-pages.md))
