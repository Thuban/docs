# Bases de la messagerie


_____

**Écrivez un message** en utilisant la boîte de texte en bas de Mattermost. Appuyez sur **ENTRÉE** pour envoyer un message. Utilisez **Majuscule + ENTRÉE** pour créer une nouvelle ligne sans envoyer de message.

**Répondez à un message** en cliquant sur la flèche de réponse à côté du texte du message.

![reply arrow](../../images/replyIcon.PNG)

**Notifiez vos coéquipiers** quand c'est nécessaire en saisissant `@pseudo`

**Formatez vos messages** en utilisant Markdown, qui prend en charge la mise en forme du texte, les titres, les liens, les émoticônes, les blocs de code, les citations, les tableaux, les listes et les images sur la même ligne.

![markdown](../../images/messagesTable1.PNG)

**Joignez des fichiers** en les glissant-déposant dans Mattermost ou en cliquant sur l'icône de pièce jointe à côté de la boîte de texte.

En apprendre plus sur :

- [Écrire des messages et des réponses](http://docs.framateam.org/help/messaging/sending-messages.html)
- [Lire les messages](http://docs.framateam.org/help/messaging/reading-messages.html)
- [Mentionner ses coéquipiers](http://docs.framateam.org/help/messaging/mentioning-teammates.html)
- [Formater ses messages en utilisant Markdown](http://docs.framateam.org/help/messaging/formatting-text.html)
- [Joindre des fichiers](http://docs.framateam.org/help/messaging/attaching-files.html)