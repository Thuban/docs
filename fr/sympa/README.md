# Framalistes

[Framalistes](https://framalistes.org)  vous permet de créer des liste de diffusion emails : toute personne s´abonnant à votre liste pourra recevoir les emails qui y sont envoyés, et y participer à son tour. À vous de choisir si cette liste est publique, semi-privée ou privée.

Le service repose sur le logiciel libre [Sympa](https://www.sympa.org/).

---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/1060l.jpg" height="340" width="570">
    <source src="https://framatube.org/blip/framalistes.mp4" type="video/mp4">
    <source src="https://framatube.org/blip/framalistes.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framalistes.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par Nicolas Geiger pour le site [collecti.cc/](http://www.collecti.cc/).